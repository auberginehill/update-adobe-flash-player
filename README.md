
## Update-AdobeFlashPlayer.ps1

|                  |                                                                                                            |
|------------------|------------------------------------------------------------------------------------------------------------|
| **OS:**          | Windows                                                                                                    |
| **Type:**        | A Windows PowerShell script                                                                                |
| **Language:**    | Windows PowerShell                                                                                         |
| **Description:** | Update-AdobeFlashPlayer downloads a list of the most recent Flash version numbers against which it compares the Flash version numbers found on the system and displays, whether a Flash update is needed or not. If a working Internet connection is not found, Update-AdobeFlashPlayer will exit at an early stage without displaying any info apart from what is found on the system. The actual update process naturally needs elevated rights, but if, however, all detected Flash Players seem to be up-to-date, Update-AdobeFlashPlayer will exit before checking, whether it is run elevated or not. Thus, if Update-AdobeFlashPlayer is run in a up-to-date machine in a "normal" PowerShell window, Update-AdobeFlashPlayer will just check that everything is OK and leave without further ceremony. |
|                  | If Update-AdobeFlashPlayer is run without elevated rights (but with a working Internet connection) in a machine with old Flash versions, it will be shown that a Flash update is needed, but Update-AdobeFlashPlayer will exit with a fail before actually downloading any files or making any changes to the system. To perform an update with Update-AdobeFlashPlayer, PowerShell has to be run in an elevated window (run as an administrator). |
|                  | If Update-AdobeFlashPlayer is run in an elevated PowerShell window and no Flash is detected, the script offers the option to install one specific version of Flash in two steps in the "Admin Corner", where, in contrary to the main autonomous nature of Update-AdobeFlashPlayer, an end-user input is required. |
|                  | In the update procedure (if old Flash has been found and Update-AdobeFlashPlayer is run with administrative rights) Update-AdobeFlashPlayer downloads the Flash uninstaller from Adobe and (a) full Flash installer(s) for the type(s) of Flash Player(s) from Adobe, which it has deemed to be outdated and after stopping several Flash-related processes uninstalls the outdated Flash version(s) and installs the downloaded Flash Player(s). Adobe Flash Player is configured by creating a backup of the exisiting configuration file (mms.cfg) and overwriting new settings to the configuration file. After the installation a web page in the default browser is opened for verifying that the Flash Player has been installed correctly. The downloaded files are purged from the hard drive after a while. This script is based on Bob Ross' PowerShell script "[Automated Adobe Flash Player Maintenance PowerScript for Public Computing Environment](http://powershell.com/cs/forums/t/6128.aspx)". |
|                  | For further information, please see the [Wiki](https://bitbucket.org/auberginehill/update-adobe-flash-player/wiki/).     |
| **Homepage:**    | https://bitbucket.org/auberginehill/update-adobe-flash-player                                                          |
|                  | Short URL: https://tinyurl.com/yynh86x2                                                                    |
| **Version:**     | 2.0                                                                                                        |
| **Downloads:**   | For instance [Update-AdobeFlashPlayer.ps1](https://bitbucket.org/auberginehill/update-adobe-flash-player/src/master/Update-AdobeFlashPlayer.ps1). Or [everything as a .zip-file](https://bitbucket.org/auberginehill/update-adobe-flash-player/downloads/). Or `git clone https://auberginehill@bitbucket.org/auberginehill/update-adobe-flash-player.git`. Or `git fetch && git checkout master`. |




### Screenshot

![Screenshot](https://bitbucket.org/auberginehill/update-adobe-flash-player/raw/8d822dc27a84b3023f517a4996cec6ea69683e90/Update-AdobeFlashPlayer.png)




### www

|                        |                                                                              |                               |
|:----------------------:|------------------------------------------------------------------------------|-------------------------------|
| :globe_with_meridians: | [Script Homepage](https://github.com/auberginehill/update-adobe-flash-player) |                              |
|                        | [Automated Adobe Flash Player Maintenance PowerScript for Public Computing Environment](http://powershell.com/cs/forums/t/6128.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20150818075129/http://powershell.com/cs/forums/t/6128.aspx)) | Bob Ross |
|                        | [Test Internet connection](http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20110612212629/http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx)) | ps1 |
|                        | [PowerTips Monthly vol 8 January 2014](http://powershell.com/cs/PowerTips_Monthly_Volume_8.pdf#IDERA-1702_PS-PowerShellMonthlyTipsVol8-jan2014) (or one of the [archive.org versions](https://web.archive.org/web/20150110213108/http://powershell.com/cs/media/p/30542.aspx)) | Tobias Weltner |
|                        | [How to run exe with/without elevated privileges from PowerShell](http://stackoverflow.com/questions/29266622/how-to-run-exe-with-without-elevated-privileges-from-powershell?rq=1) | alejandro5042 |
|                        | [A little powershell help, Flash Version Query](https://community.spiceworks.com/topic/487699-a-little-powershell-help-flash-version-query) | Raven Hunter |
|                        | [Get current versions of Adobe Products](https://www.reddit.com/r/PowerShell/comments/3tgr2m/get_current_versions_of_adobe_products/) | Kreloc |
|                        | [Flash Player Plugin](https://chocolatey.org/packages/flashplayerplugin)     | chocolatey                    |
|                        | [What's the best way to determine the location of the current PowerShell script?](http://stackoverflow.com/questions/5466329/whats-the-best-way-to-determine-the-location-of-the-current-powershell-script?noredirect=1&lq=1) | JaredPar and Matthew Pirocchi |
|                        | [Creating a Menu](http://powershell.com/cs/forums/t/9685.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20150910111758/http://powershell.com/cs/forums/t/9685.aspx)) | lamaar75 |
|                        | [Perfect Progress Bars for PowerShell](https://www.credera.com/blog/technology-insights/perfect-progress-bars-for-powershell/) | |
|                        | [Adding a Simple Menu to a Windows PowerShell Script](https://technet.microsoft.com/en-us/library/ff730939.aspx) | |
|                        | [Uninstall method of the Win32\_Product class](https://msdn.microsoft.com/en-us/library/aa393941(v=vs.85).aspx) | |
|                        | [http://www.figlet.org/](http://www.figlet.org/) and [ASCII Art Text Generator](http://www.network-science.de/ascii/) | ASCII Art |
|                        | [HTML To Markdown Converter](https://digitalconverter.azurewebsites.net/HTML-to-Markdown-converter) |        |
|                        | [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables) |                               |
|                        | [HTML table syntax into Markdown](https://jmalarcon.github.io/markdowntables/) |                             |
|                        | [A HTML to Markdown converter written in JavaScript](https://domchristie.github.io/turndown/) | Dom Christie |
|                        | [Paste to Markdown](https://euangoddard.github.io/clipboard2markdown/)       |                               |
|                        | [Convert HTML or anything to Markdown](https://cloudconvert.com/html-to-md)  |                               |
