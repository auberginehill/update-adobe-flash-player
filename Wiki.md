```
  _    _           _       _                        _       _          ______ _           _     _____  _
 | |  | |         | |     | |              /\      | |     | |        |  ____| |         | |   |  __ \| |
 | |  | |_ __   __| | __ _| |_ ___ ______ /  \   __| | ___ | |__   ___| |__  | | __ _ ___| |__ | |__) | | __ _ _   _  ___ _ __
 | |  | | '_ \ / _` |/ _` | __/ _ \______/ /\ \ / _` |/ _ \| '_ \ / _ \  __| | |/ _` / __| '_ \|  ___/| |/ _` | | | |/ _ \ '__|
 | |__| | |_) | (_| | (_| | ||  __/     / ____ \ (_| | (_) | |_) |  __/ |    | | (_| \__ \ | | | |    | | (_| | |_| |  __/ |
  \____/| .__/ \__,_|\__,_|\__\___|    /_/    \_\__,_|\___/|_.__/ \___|_|    |_|\__,_|___/_| |_|_|    |_|\__,_|\__, |\___|_|
        | |                                                                                                     __/ |
        |_|                                                                                                    |___/             
```

# Update-AdobeFlashPlayer Wiki

[TOC]

## Screenshot

![Screenshot](https://bitbucket.org/auberginehill/update-adobe-flash-player/raw/8d822dc27a84b3023f517a4996cec6ea69683e90/Update-AdobeFlashPlayer.png)



## Outputs 

:arrow_right: Displays Flash related information in console.

  - Tries to update outdated Adobe Flash Player(s) to its/their latest version(s), if old Flash Player(s) is/are found and if Update-AdobeFlashPlayer is run in an elevated Powershell window. In addition to that, if such an update procedure is initiated...

  - The Flash Player configuration file (`mms.cfg`) is overwritten with new parameters and the following backups are made:

    **Configuration file:**

    | System         | File                                       |
    |----------------|--------------------------------------------|
    | 32-bit Windows | `%WINDIR%\System32\Macromed\Flash\mms.cfg` |
    | 64-bit Windows | `%WINDIR%\SysWow64\Macromed\Flash\mms.cfg` |

    **"Original" file**, which is created when the script tries to update something for the first time:

    | System         | File                                                |
    |----------------|-----------------------------------------------------|
    | 32-bit Windows | `%WINDIR%\System32\Macromed\Flash\mms_original.cfg` |
    | 64-bit Windows | `%WINDIR%\SysWow64\Macromed\Flash\mms_original.cfg` |

    **"Backup" file**, which is created when the script tries to update something for the second time and which gets overwritten in each successive update cycle:

    | System         | File                                              |
    |----------------|---------------------------------------------------|
    | 32-bit Windows | `%WINDIR%\System32\Macromed\Flash\mms_backup.cfg` |
    | 64-bit Windows | `%WINDIR%\SysWow64\Macromed\Flash\mms_backup.cfg` |

     The `%WINDIR%` location represents the Windows system directory, such as `C:\Windows` and may be displayed in PowerShell with the `$env:windir` variable.

  - To see the actual values that are being written, please see the Step 6 in the [script](https://bitbucket.org/auberginehill/update-adobe-flash-player/src/master/Update-AdobeFlashPlayer.ps1) itself, where the original settings are overwritten with the following values:

    | Value                        | Description                                             |
    |------------------------------|---------------------------------------------------------|
    | `# [AssetCacheSize = 0`      | Disables storing the common Flash components            |
    | `AutoUpdateDisable = 1`      | Disables the Automatic Flash Update                     |
    | `LegacyDomainMatching = 0`   | Denies Flash Player 6 and earlier superdomain rules     |
    | `LocalFileLegacyAction = 0`  | Denies Flash Player 7 and earlier local-trusted sandbox |
    | `# [LocalStorageLimit = 1`   | Disables persistent shared objects                      |
    | `SilentAutoUpdateEnable = 0` | Disables background updates                             |
    | `# [ThirdPartyStorage = 0`   | Denies third-party locally persistent shared objects    |

    Most of the settings above may render some web pages broken.

    Lines marked with # are written only if the symbol # is removed from the beginning of the appropriate line inside the source code section (please see the Step 6 in the [script](https://bitbucket.org/auberginehill/update-adobe-flash-player/src/master/Update-AdobeFlashPlayer.ps1) itself, about at line ~1111).

    For a comprehensive list of available settings and a more detailed description of the values above, please see the [Adobe Flash Player Administration Guide](http://www.adobe.com/devnet/flashplayer/articles/flash_player_admin_guide.html).

  - To open these file locations in a Resource Manager Window, for instance a command...
     
    `Invoke-Item $env:windir\System32\Macromed\Flash\`  

    or  
    
    `Invoke-Item $env:windir\SysWOW64\Macromed\Flash\`  
   
    ...may be used at the PowerShell prompt window `[PS>]`.




## Notes 

:warning: Requires a working Internet connection for downloading a list of the most recent Flash version numbers.

  - Also requires a working Internet connection for downloading a Flash uninstaller and a complete Flash installer(s) from Adobe (but this procedure is not initiated, if the system is deemed up-to-date).

  - For performing any actual updates with Update-AdobeFlashPlayer, it's mandatory to run this script in an elevated PowerShell window (where PowerShell has been started with the "run as an administrator" option). The elevated rights are needed for uninstalling Flash, installing Flash and for writing the `mms.cfg` file.

  - Please also notice that during the actual update phase Update-AdobeFlashPlayer closes a bunch of processes without any further notice in Step 3 and in Step 6 Update-AdobeFlashPlayer alters the Flash configuration file (`mms.cfg`) so, that for instance, the automatic Flash updates are turned off.

  - The Flash Player ActiveX control on Windows 8.1 and above is a component of Internet Explorer and Edge and is updated via Windows Update. By using the Flash Player ActiveX installer, Flash Player ActiveX control cannot be installed on Windows 8.1 and above systems. Also, the Flash Player uninstaller doesn't uninstall the ActiveX control on Windows 8.1 and above systems.

  - Please note that when run in an elevated PowerShell window and old Flash Player(s) is/are detected, Update-AdobeFlashPlayer will automatically try to download files from the Internet without prompting the end-user beforehand or without asking any confirmations (in Step 1 and Step 2).

  - Please note that the downloaded files are temporarily placed in a directory, which is specified with the `$path` variable (at line 41).

  - The `$env:temp` variable points to the current temp folder. The default value of the `\$env:temp` variable is `C:\Users<username\>\AppData\Local\Temp` (i.e. each user account has their own separate temp folder at path `%USERPROFILE%\AppData\Local\Temp`). To see the current temp path, for instance a command...

    `[System.IO.Path]::GetTempPath()`

    ...may be used at the PowerShell prompt window `[PS\>]`. 

  - To change the temp folder for instance to `C:\Temp`, please, for example, follow the instructions at [Temporary Files Folder - Change Location in Windows](http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html), which in essence are something along the lines:
    1. Right click Computer icon and select Properties (or select Start → Control Panel → System. On Windows 10 this instance may also be found by right clicking Start and selecting Control Panel → System... or by pressing `[Win-key]` + X and selecting Control Panel → System). On the window with basic information about the computer...
    1. Click on Advanced system settings on the left panel and select Advanced tab on the "System Properties" pop-up window.
    1. Click on the button near the bottom labeled Environment Variables.
    1. In the topmost section, which lists the User variables, both TMP and TEMP may be seen. Each different login account is assigned its own temporary locations. These values can be changed by double clicking a value or by highlighting a value and selecting Edit. The specified path will be used by Windows and many other programs for temporary files. It's advisable to set the same value (a directory path) for both TMP and TEMP.
    1. Any running programs need to be restarted for the new values to take effect. In fact, probably Windows itself needs to be restarted for it to begin using the new values for its own temporary files.




## Examples

:book: To open this code in Windows PowerShell, for instance:

  1. `./Update-AdobeFlashPlayer`

    Runs the script. Please notice to insert `./` or `.\` before the script name.

  1. `help ./Update-AdobeFlashPlayer -Full`

    Displays the help file.

  1. `New-Item -ItemType File -Path C:\Temp\Update-AdobeFlashPlayer.ps1`

    Creates an empty ps1-file to the `C:\Temp` directory. The `New-Item` cmdlet has an inherent `-NoClobber` mode built into it, so that the procedure will halt, if overwriting (replacing the contents) of an existing file is about to happen. Overwriting a file with the `New-Item` cmdlet requires using the `Force`. If the path name and/or the filename includes space characters, please enclose the whole `-Path` parameter value in quotation marks (single or double): `New-Item -ItemType File -Path "C:\Folder Name\Update-AdobeFlashPlayer.ps1"`. For more information, please type "`help New-Item -Full`".

  1. `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine`

    This command is altering the Windows PowerShell rights to enable script execution in the default (`LocalMachine`) scope, and defines the conditions under which Windows PowerShell loads configuration files and runs scripts in general. In Windows Vista and later versions of Windows, for running commands that change the execution policy of the `LocalMachine` scope, Windows PowerShell has to be run with elevated rights (Run as Administrator). The default policy of the default (`LocalMachine`) scope is "`Restricted`", and a command "`Set-ExecutionPolicy Restricted`" will "undo" the changes made with the original example above (had the policy not been changed before...). Execution policies for the local computer (`LocalMachine`) and for the current user (`CurrentUser`) are stored in the registry (at for instance the `HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy` key), and remain effective until they are changed again. The execution policy for a particular session (`Process`) is stored only in memory, and is discarded when the session is closed.

    |                | PowerShell Execution Policy Parameters                                         |
    |----------------|--------------------------------------------------------------------------------|
    | `Restricted`   | Does not load configuration files or run scripts, but permits individual commands. `Restricted` is the default execution policy. |
    | `AllSigned`    | Scripts can run. Requires that all scripts and configuration files be signed by a trusted publisher, including the scripts that have been written on the local computer. Risks running signed, but malicious, scripts. |
    | `RemoteSigned` | Requires a digital signature from a trusted publisher on scripts and configuration files that are downloaded from the Internet (including e-mail and instant messaging programs). Does not require digital signatures on scripts that have been written on the local computer. Permits running unsigned scripts that are downloaded from the Internet, if the scripts are unblocked by using the `Unblock-File` cmdlet. Risks running unsigned scripts from sources other than the Internet and signed, but malicious, scripts. |
    | `Unrestricted` | Loads all configuration files and runs all scripts. Warns the user before running scripts and configuration files that are downloaded from the Internet. Not only risks, but actually permits, eventually, runningany unsigned scripts from any source. Risks running malicious scripts. |
    | `Bypass`       | Nothing is blocked and there are no warnings or prompts. Not only risks, but actually permits running any unsigned scripts from any source. Risks running malicious scripts. |
    | `Undefined`    | Removes the currently assigned execution policy from the current scope. If the execution policy in all scopes is set to `Undefined`, the effective execution policy is `Restricted`, which is the default execution policy. This parameter will not alter or remove the ("master") execution policy that is set with a Group Policy setting. |
    | **Notes:**     | Please note that the Group Policy setting "`Turn on Script Execution`" overrides the execution policies set in Windows PowerShell in all scopes. To find this ("master") setting, please, for example, open the Local Group Policy Editor (`gpedit.msc`) and navigate to Computer Configuration → Administrative Templates → Windows Components → Windows PowerShell. The Local Group Policy Editor (`gpedit.msc`) is not available in any Home or Starter edition of Windows. |
    |                | For more information, please type "`Get-ExecutionPolicy -List`", "`help Set-ExecutionPolicy -Full`", "`help about_Execution_Policies`" or visit [Set-ExecutionPolicy](https://technet.microsoft.com/en-us/library/hh849812.aspx) or [About Execution Policies](http://go.microsoft.com/fwlink/?LinkID=135170). |




## Contributing

|        |                           |                                                                                                                        |
|:------:|---------------------------|------------------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/update-adobe-flash-player/issues/).               |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/update-adobe-flash-player/issues/).   |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/update-adobe-flash-player/pull-requests/). |

For further information, please see the [Contributing.md](https://bitbucket.org/auberginehill/update-adobe-flash-player/wiki/Contributing.md) page.




## Wiki Features

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

```
$ git clone https://auberginehill@bitbucket.org/auberginehill/update-adobe-flash-player.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.